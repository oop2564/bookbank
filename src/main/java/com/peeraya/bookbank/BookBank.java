/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peeraya.bookbank;

/**
 *
 * @author Administrator
 */
public class BookBank {
    String name;
    double balance;
    
    //Constructor methods
    BookBank(String name, double money) {
        this.name = name;
        this.balance = money;
    }
    
    void deposit(double money) {
        if (money <= 0) {
            System.out.println("Money must be more than 0 !!");
            return;
        }
        this.balance += money;
    }
    
    void widthdraw(double money) {
        if (money <= 0) {
            System.out.println("Money must be more than 0 !!");
            return;
        }
        if(money > this.balance) {
            System.out.println("Not enough money");
            return;
        }
        this.balance -= money;
    }
}
